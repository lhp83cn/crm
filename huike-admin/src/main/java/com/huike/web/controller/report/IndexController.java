package com.huike.web.controller.report;


import com.huike.review.dto.TodayIndoDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.huike.common.core.domain.AjaxResult;
import com.huike.report.service.IReportService;

@RestController
@RequestMapping("/index")
@Slf4j
public class IndexController {

    @Autowired
    private IReportService reportService;


    /**
     * 首页--基础数据统计
     * @param beginCreateTime
     * @param endCreateTime
     * @return
     */
    @GetMapping("/getBaseInfo")
    public AjaxResult getBaseInfo(@RequestParam("beginCreateTime") String beginCreateTime,
                                  @RequestParam("endCreateTime") String endCreateTime){
        return AjaxResult.success(reportService.getBaseInfo(beginCreateTime,endCreateTime));
    }

    /**
     * 获取今天信息
     *
     * @return {@link AjaxResult}
     */
    @GetMapping("/getTodayInfo")
    public AjaxResult getTodayInfo(){
        log.info("获取今日简报");
        TodayIndoDTO todayInfo= reportService.todayInfo();
        return AjaxResult.success(todayInfo);
    }


}
