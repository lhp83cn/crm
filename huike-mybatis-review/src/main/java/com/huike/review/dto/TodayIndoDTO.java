package com.huike.review.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TodayIndoDTO {

    /*"todayCluesNum":0, //今日线索数目
        "todayBusinessNum":0,//今日商机数目
        "todayContractNum":1,//今日合同数目
        "todaySalesAmount":0 //今日销售金额 */
    private Long todayCluesNum;
    private Long todayBusinessNum;
    private Long todayContractNum;
    private Long todaySalesAmount;
}
